(() => {
  /***** OS DOWNLOAD BUTTON *****/

  let os = null;
  let dl = null;
  function uaContains(str) {
    return navigator.userAgent.indexOf(str) != -1;
  }
  if (uaContains("X11")) {
    os = "Linux";
    dl = "hourglass-linux.tar.gz";
  } else if (uaContains("Mac") && !uaContains("iPad") && !uaContains("iPhone")) {
    os = "macOS";
    dl = "hourglass-macos.zip";
  } else if (uaContains("Win")) {
    os = "Windows";
    dl = "hourglass-windows.zip";
  }

  if (os) {
    let button = document.getElementById("download-button");
    button.innerText = `Download for ${os}`;
    button.href = `https://jwestman.gitlab.io/hourglass/${dl}`;
    button.classList.remove("display-none");
  }

  /***** SCREENSHOT GALLERY *****/
  let screenshots = document.getElementById("screenshots");
  let thumbnails = document.getElementById("screenshots-thumbnails");

  function showChild(screenshot, link) {
    for (let child of screenshots.children) {
      child.setAttribute("aria-expanded", false);
      child.hidden = true;
    }
    for (let child of thumbnails.children) {
      child.setAttribute("aria-selected", false);
    }
    screenshot.setAttribute("aria-expanded", true);
    screenshot.hidden = false;
    link.setAttribute("aria-selected", true);
  }

  let id = 0;
  for (let child of screenshots.children) {
    id ++;

    child.setAttribute("role", "tabpanel");
    child.id = "screenshot-" + id;

    let link = document.createElement("button");
    link.setAttribute("role", "tab");
    link.setAttribute("aria-selected", id == 1);
    link.setAttribute("aria-label", "Screenshot " + id);

    link.onclick = () => {
      showChild(child, link);
    };
    if (id === 1) {
      showChild(child, link);
    }

    let thumb = document.createElement("img");
    thumb.src = child.src;
    thumb.alt = "";
    link.appendChild(thumb);
    thumbnails.appendChild(link);
  }
})();
